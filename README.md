# flowcontrol
Hier findest du den Quellcode zu der flowcontrol Modifikation von Bürkert. Diesen Quellcode kannst du dir [hier](https://gitlab.com/buerkert/flowcontrol/-/archive/master/flowcontrol-master.zip) herunterladen und weiterentwickeln. Weitere Informationen zu der Modifikation findest du [hier](https://tilweb.burkert.com).

## Ausbildungsprojekt
Dies ist ein Ausbildungsprojekt der Bürkert Werke GmbH & Co KG. An der Programmierung selbst, der begleitenden Webseite sowie den organisatorischen Themen rund um dieses Projekt arbeiten unsere Studenten und Auszubildenden und lernen so viele verschiedene Themen des Arbeitsalltags kennen. Wir bei Bürkert legen viel Wert auf eine gute Ausbildung, dies beweisen uns auch die Auszeichnungen, welche uns als eines der besten Ausbildungsunternehmen Deutschlands auszeichnen.

Mehr Informationen dazu findet Ihr auch auf unserem Ausbildungsblog: [https://www.buerkert.de/de/Unternehmen-Karriere/Aktuelles/Ausbildungsblog](https://www.buerkert.de/de/Unternehmen-Karriere/Aktuelles/Ausbildungsblog) sowie auf den Informationsseiten zum Thema Ausbildung und Studium [https://www.buerkert.de/de/Unternehmen-Karriere/Karriere/Ausbildung-Studium-bei-Buerkert](https://www.buerkert.de/de/Unternehmen-Karriere/Karriere/Ausbildung-Studium-bei-Buerkert).

Nun aber zur eigentlich Minecraft Mod:

## Schritt-für-Schritt Anleitung
Falls du ohne Git arbeiten möchtest, kannst du das Projekt [hier](https://gitlab.com/buerkert/flowcontrol/-/archive/master/flowcontrol-master.zip) downloaden. Die Zip-Datei entpackst du dann in deinem gewünschten Projektverzeichnis und folgst der unten beschriebenen Anleitung ab Schritt 2.

**Terminal-Basierend**:
1. Git Repository klonen ``git clone https://gitlab.com/buerkert/flowcontrol.git``.
2. Minecraft-Dateien herunterladen und für das Programmieren vorbereiten ``gradlew setupDecompWorkspace``.
3. Zusätzliche Dateien für das Ausführen der Modifikation in der gewählten IDE herunterladen. Falls Eclipse verwendet wird ``gradlew eclipse`` ausführen, falls IntelliJ verwendet wird ``gradlew getIntellijRuns`` ausführen. Nun kann man das Projekt in der Entwicklungsumgebung öffnen.
4. Um den Minecraft-Client zu starten, führt man ``gradlew runClient`` aus. Falls man den Server starten will, führt man ``gradlew runServer`` aus, dieser läuft dann als localhost.
5. Damit du die Modifikation im normalen Minecraft Launcher verwenden kannst, musst du das Projekt erst einmal kompilieren. Hierfür führe ``gradlew build`` aus. In dem Unterordner des Projekts **build/libs** sollte nun eine **flowontrol-[version].jar** zu finden sein. Diese kannst du wie jede andere Modifikation in den mods-Ordner
deines Minecraftverzeichnisses legen.

**Terminal-Freie IntelliJ IDEA Konfiguration**:
1. Öffne hierfür unter **File > New > Project from Version Control > Git** und klone das Git Repository ``https://gitlab.com/buerkert/flowcontrol.git``.
2. Unter **Gradle > Tasks > forgegradle > setupDecompWorkspace** ausführen um Minecraft-Dateien herunterzuladen.
3. Unter **Gradle > Tasks > forgegradle > getIntellijRuns** ausführen um zusätzliche Dateien für das Ausführen der Modifikation herunterzuladen. Dabei kann es vorkommen, dass der RAM nicht ausreicht und man Gradle mehr RAM zuweisen muss. Um das zu verhindern, kann man Gradle in den Einstellungen unter Gradle VM options mehr RAM zuweisen, beispielsweise ``-Xmx3G`` für 3 Gigabyte.
4. Damit man die Modifikation ausführen kann, muss man jetzt nur noch die Runs konfigurieren, dazu öffnet man **Edit configuration** und wählt bei beiden den Klassenpfad mit ``flowcontrol.main`` aus.
5. Für das Kompilieren von Minecraft gehst du auch hierfür unter **Gradle > Tasks > build** und fürhst **build** aus. In dem Unterordner des Projekts **build/libs** sollte nun eine **flowontrol-[version].jar** zu finden sein. Diese kannst du wie jede andere Modifikation in den mods-Ordner deines Minecraftverzeichnisses legen. 


package de.buerkert.flowcontrol.flowsystem;

import net.minecraft.block.state.IBlockState;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;

public interface IFlowBlockProperties extends IBlockState {
    int getStrongStrength(IBlockAccess blockAccess, BlockPos pos, EnumFacing side);
    boolean canProvideWater();
}

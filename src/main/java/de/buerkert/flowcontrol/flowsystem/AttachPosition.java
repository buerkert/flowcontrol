package de.buerkert.flowcontrol.flowsystem;

import net.minecraft.util.IStringSerializable;

public enum AttachPosition implements IStringSerializable {
    UP("up"),
    SIDE("side"),
    NONE("none");

    private final String name;

    AttachPosition(String name) {
        this.name = name;
    }

    public String toString() {
        return this.getName();
    }

    public String getName() {
        return this.name;
    }
}

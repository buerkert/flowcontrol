package de.buerkert.flowcontrol.flowsystem;

import de.buerkert.flowcontrol.block.FlowControlBlock;
import de.buerkert.flowcontrol.block.ProductBlock;
import de.buerkert.flowcontrol.block.flowsystem.Repeater;
import de.buerkert.flowcontrol.block.flowsystem.Tube;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.Objects;

public class FlowWorld {

    public static boolean isSideWatered(World worldIn, BlockPos pos, EnumFacing facing) {
        return getWaterPower(worldIn, pos, facing) > 0;
    }

    public static int getWaterPower(World worldIn, BlockPos pos, EnumFacing facing) {
        int strength = 0;
        IBlockState iBlockState = worldIn.getBlockState(pos);
        if(iBlockState.getBlock().equals(FlowControlBlock.TUBE.getBlock())) {
            strength = (Integer)iBlockState.getValue(Tube.STRENGTH);
            return strength - 1;
        }
        if(iBlockState.getBlock().equals(FlowControlBlock.REPEATER.getBlock()) && iBlockState.getValue(Repeater.FACING) == facing && (Boolean)iBlockState.getValue(Repeater.POWER)) {
            if((Boolean)iBlockState.getValue(Repeater.EXTENSION)) {
                return 15;
            }
            return 7;
        }
        if(iBlockState.getBlock().equals(FlowControlBlock.PUMP.getBlock())) {
            return 15;
        }
        if(Objects.requireNonNull(iBlockState.getBlock().getRegistryName()).toString().contains("product")) {
            IBlockState blockStateBehind = worldIn.getBlockState(pos.offset(facing, 1));
            if(blockStateBehind.getBlock().equals(FlowControlBlock.TUBE.getBlock())) {
                int tStrength = (Integer)blockStateBehind.getValue(Tube.STRENGTH);
                if(tStrength > 0) {
                    strength = iBlockState.getValue(ProductBlock.FLOW);
                    if(strength > tStrength) {
                        strength = tStrength;
                        worldIn.setBlockState(pos, iBlockState.withProperty(ProductBlock.FLOW, strength), 3);
                    }
                }
            }
            if(blockStateBehind.getBlock().equals(FlowControlBlock.REPEATER.getBlock())) {
                strength = iBlockState.getValue(ProductBlock.FLOW);
                if(strength >= 3) {
                    strength = 3;
                }
            }
        }
        return strength;
    }

    public static boolean isBlockWatered(World worldIn, BlockPos pos) {
        if(getWaterPower(worldIn, pos.down(), EnumFacing.DOWN) > 0) {
            return true;
        } else if(getWaterPower(worldIn, pos.up(), EnumFacing.UP) > 0) {
            return true;
        } else if(getWaterPower(worldIn, pos.north(), EnumFacing.NORTH) > 0) {
            return true;
        } else if(getWaterPower(worldIn, pos.south(), EnumFacing.SOUTH) > 0) {
            return true;
        } else if(getWaterPower(worldIn, pos.west(), EnumFacing.WEST) > 0) {
            return true;
        } else {
            return getWaterPower(worldIn, pos.east(), EnumFacing.EAST) > 0;
        }
    }

    public static int isBlockIndirectGettingWatered(World worldIn, BlockPos pos) {
        int j = 0;
        for(EnumFacing enumFacing : EnumFacing.values()) {
            int k = getWaterPower(worldIn, pos.offset(enumFacing), enumFacing);
            if(k >= 15) {
                return 15;
            }
            if(k > j) {
                j = k;
            }
        }
        return j;
    }

    public static int getStrongStrength(World worldIn, BlockPos pos, EnumFacing direction) {
        IFlowBlockState iFlowBlockState = (IFlowBlockState)worldIn.getBlockState(pos);
        return iFlowBlockState.getStrongStrength(worldIn, pos, direction);
    }

    public static int getStrongStrength(World worldIn, BlockPos pos) {
        int j = 0;
        j = Math.max(j, getStrongStrength(worldIn, pos.down(), EnumFacing.DOWN));
        if(j >= 15) {
            return j;
        } else {
            j = Math.max(j, getStrongStrength(worldIn, pos.up(), EnumFacing.UP));
            if(j >= 15) {
                return j;
            } else {
                j = Math.max(j, getStrongStrength(worldIn, pos.north(), EnumFacing.NORTH));
                if(j >= 15) {
                    return j;
                } else {
                    j = Math.max(j, getStrongStrength(worldIn, pos.south(), EnumFacing.SOUTH));
                    if(j >= 15) {
                        return j;
                    } else {
                        j = Math.max(j, getStrongStrength(worldIn, pos.west(), EnumFacing.WEST));
                        if(j >= 15) {
                            return j;
                        } else {
                            j = Math.max(j, getStrongStrength(worldIn, pos.east(), EnumFacing.EAST));
                            return j >= 15 ? j : j;
                        }
                    }
                }
            }
        }
    }
}

package de.buerkert.flowcontrol.flowsystem;

import com.google.common.collect.ImmutableMap;
import net.minecraft.block.Block;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.IBlockBehaviors;
import net.minecraft.block.state.IBlockProperties;
import net.minecraft.block.state.IBlockState;

import java.util.Collection;

public interface IFlowBlockState extends IBlockBehaviors, IBlockProperties, IFlowBlockProperties {
    Collection<IProperty<? >> getPropertyKeys();

    /**
     * Get the value of the given Property for this BlockState
     */
    <T extends Comparable<T>> T getValue(IProperty<T> property);

    /**
     * Get a version of this BlockState with the given Property now set to the given value
     */
    <T extends Comparable<T>, V extends T>IBlockState withProperty(IProperty<T> property, V value);

    /**
     * Create a version of this BlockState with the given property cycled to the next value in order. If the property
     * was at the highest possible value, it is set tot the lowest one instead.
     */
    <T extends Comparable<T>> IBlockState cycleProperty(IProperty<T> property);

    ImmutableMap< IProperty<?>, Comparable<? >> getProperties();

    Block getBlock();
}

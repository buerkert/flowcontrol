package de.buerkert.flowcontrol.creativetab;

import de.buerkert.flowcontrol.block.FlowControlBlock;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class FlowSystemTab extends CreativeTabs {
    public FlowSystemTab(String label) {
        super(label);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public ItemStack getTabIconItem() {
        return new ItemStack(FlowControlBlock.TUBE.getBlock());
    }
}

package de.buerkert.flowcontrol;


import com.google.common.collect.Lists;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;

import java.util.ArrayList;
import java.util.List;

public class Rotations {

    public static List<AxisAlignedBB> rotateAxisAlignedBB(EnumFacing facing, List<AxisAlignedBB> axisAlignedBBList) {
        List<AxisAlignedBB> newCollisionBoxes = new ArrayList<>();
        for(AxisAlignedBB axisAlignedBB : axisAlignedBBList) {

            double[] min = switchXZ(axisAlignedBB.minX, axisAlignedBB.minZ, facing);
            double[] max = switchXZ(axisAlignedBB.maxX, axisAlignedBB.maxZ, facing);

            newCollisionBoxes.add(new AxisAlignedBB(min[0], axisAlignedBB.minY, min[1], max[0], axisAlignedBB.maxY, max[1]));
        }
        return Lists.newArrayList(newCollisionBoxes);
    }

    public static AxisAlignedBB rotateAxisAlignedBB(EnumFacing facing, AxisAlignedBB axisAlignedBB) {
        double[] min = switchXZ(axisAlignedBB.minX, axisAlignedBB.minZ, facing);
        double[] max = switchXZ(axisAlignedBB.maxX, axisAlignedBB.maxZ, facing);
        return new AxisAlignedBB(min[0], axisAlignedBB.minY, min[1], max[0], axisAlignedBB.maxY, max[1]);
    }

    private static double[] switchXZ(double x, double z,  EnumFacing facing) {
        switch (facing) {
            case SOUTH:
                return new double[]{x, z};
            case NORTH:
                return new double[]{1-x, 1-z};
            case EAST:
                return new double[]{z, x};
            case WEST:
                return new double[]{1-z, 1-x};
        }
        return new double[]{0, 0};
    }
}

package de.buerkert.flowcontrol.gui;

import de.buerkert.flowcontrol.block.ProductBlock;
import de.buerkert.flowcontrol.gui.helper.Format;
import de.buerkert.flowcontrol.gui.helper.Responder;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiLabel;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiSlider;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.lwjgl.input.Keyboard;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;

public class ProductGui extends GuiScreen {
    private GuiButton buttonBack;
    private GuiButton buttonSubmit;
    private GuiButton buttonWeb;
    private ProductBlock productBlock;
    private World worldIn;
    private BlockPos pos;
    private IBlockState state;
    private EntityPlayer playerIn;
    private Responder responder = new Responder();
    private Format helper = new Format();
    private GuiSlider guiSlider;

    private GuiLabel propertiesLabel;

    public ProductGui(ProductBlock productBlock, World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn) {
        this.productBlock = productBlock;
        this.worldIn = worldIn;
        this.pos = pos;
        this.state = state;
        this.playerIn = playerIn;
    }

    @Override
    public void initGui() {
        buttonList.clear();
        int widthScale = width / 100;
        int heightScale = height / 100;
        Keyboard.enableRepeatEvents(true);
        this.setGuiSize(width, height);

        propertiesLabel = new GuiLabel(fontRenderer, 0, widthScale * 5, heightScale * 5, widthScale * 40, heightScale * 80, Integer.parseInt("FFFFFF", 16));
        propertiesLabel.addLine(net.minecraft.client.resources.I18n.format("gui.properties.name"));
        int i = 0;
        StringBuilder propertyString = new StringBuilder();
        for(String property : productBlock.getProperties()) {
            if(i == 0) {
                propertyString = new StringBuilder(net.minecraft.client.resources.I18n.format("gui.properties." + property + ".name"));
                i++;
                continue;
            }
            propertyString.append(": ").append(property);
            propertiesLabel.addLine(propertyString.toString());
            propertyString.delete(0, propertyString.length());
            i = 0;
        }

        guiSlider = addButton(new GuiSlider(responder, 0, width/2 + widthScale * 5, heightScale * 25, "flow", 0, 3, productBlock.getFlow(state), helper));
        buttonBack = addButton(new GuiButton(0, width - widthScale * 20, height - heightScale * 12, widthScale * 15, heightScale * 7, net.minecraft.client.resources.I18n.format("gui.button.back.name")));
        buttonSubmit = addButton(new GuiButton(1, width - widthScale * 40, height - heightScale * 12, widthScale * 15, heightScale * 7, net.minecraft.client.resources.I18n.format("gui.button.submit.name")));
        buttonWeb = addButton(new GuiButton(1, width - widthScale * 20, height - heightScale * 24, widthScale * 15, heightScale * 7, net.minecraft.client.resources.I18n.format("gui.button.web.name")));
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        drawDefaultBackground();
        drawCenteredString(fontRenderer, I18n.format(productBlock.getUnlocalizedName() + ".name"), width / 2, height / 8 - 4, Integer.parseInt("FFFFFF", 16));
        propertiesLabel.drawLabel(mc, mouseX, mouseY);
        super.drawScreen(mouseX, mouseY, partialTicks);
    }

    @Override
    public void updateScreen() {
        super.updateScreen();
    }

    @Override
    public boolean doesGuiPauseGame() {
        return false;
    }

    @Override
    public void onGuiClosed() {
        Keyboard.enableRepeatEvents(false);
    }

    @Override
    protected void keyTyped(char typedChar, int keyCode) throws IOException {
        super.keyTyped(typedChar, keyCode);
    }

    @Override
    protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
        super.mouseClicked(mouseX, mouseY, mouseButton);
    }

    @Override
    public void actionPerformed(GuiButton button) {
        if(button.equals(buttonBack)) mc.displayGuiScreen(null);
        if(button.equals(buttonWeb)) openWebLink(URI.create(net.minecraft.client.resources.I18n.format(productBlock.websiteI18n)));
        if(button.equals(buttonSubmit)) {
            int flow = (int)Math.round(guiSlider.getSliderValue());
            worldIn.setBlockState(pos, state.withProperty(ProductBlock.FLOW, 0), 3);
            worldIn.setBlockState(pos, state.withProperty(ProductBlock.FLOW, flow), 3);
            worldIn.updateBlockTick(pos, state.getBlock(), 3, 1);
        }
    }

    private void openWebLink(URI url)
    {
        try
        {
            Class<?> oclass = Class.forName("java.awt.Desktop");
            Object object = oclass.getMethod("getDesktop").invoke((Object)null);
            oclass.getMethod("browse", URI.class).invoke(object, url);
        } catch (InvocationTargetException | NoSuchMethodException | IllegalAccessException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}

package de.buerkert.flowcontrol.item;

import de.buerkert.flowcontrol.Main;
import net.minecraft.item.Item;

import java.util.HashMap;
import java.util.Map;

public enum FlowControlItem {
    CABLE("cable"),
    CASE("case"),
    CIRCUIT("circuit"),
    COPPER_INGOT("copper_ingot"),
    COPPER_NUGGET("copper_nugget"),
    MEMBRANE("membrane"),
    QUILL("quill"),
    RUBBER("rubber"),
    WIRE("wire")
    ;

    private static final Map<String, FlowControlItem> ITEM_MAP = new HashMap<>();
    private String name;
    private Item item;

    static {
        for(FlowControlItem item : values()) {
            ITEM_MAP.put(item.name, item);
        }
    }

    FlowControlItem(String name) {
        this.name = name;
        this.item = new Item()
                .setRegistryName("item_" + name)
                .setUnlocalizedName("item_" + name)
                .setCreativeTab(Main.BUERKERT_TAB);
    }

    @Override
    public String toString() {
        return this.getName();
    }

    private String getName() {
        return this.name;
    }

    public Item getItem() {
        return this.item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public static FlowControlItem fromName(String name) {
        for(Map.Entry<String, FlowControlItem> entry : ITEM_MAP.entrySet()) {
            if (!entry.getKey().equalsIgnoreCase(name)) {
                continue;
            }
            return entry.getValue();
        }
        return null;
    }
}

package de.buerkert.flowcontrol;

import de.buerkert.flowcontrol.block.FlowControlBlock;
import de.buerkert.flowcontrol.item.FlowControlItem;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.common.registry.GameRegistry;

import java.util.Objects;

public class Registry {
    private static int DEFAULT_ITEM_SUBTYPE = 0;

    public static void initialize() {
        for(FlowControlBlock block : FlowControlBlock.values()) {
            registerBlock(block.getBlock());
        }
        for(FlowControlItem item : FlowControlItem.values()) {
            registerItem(item.getItem());
        }
        registerAllSmeltingRecipes();
    }

    private static void registerBlock(Block block) {
        ItemBlock itemBlock = new ItemBlock(block);
        itemBlock.setRegistryName(Objects.requireNonNull((block).getRegistryName()));

        ForgeRegistries.BLOCKS.register(block);
        ForgeRegistries.ITEMS.register(itemBlock);

        ModelResourceLocation modelResourceLocation = new ModelResourceLocation(block.getRegistryName(), "inventory");
        ModelLoader.setCustomModelResourceLocation(itemBlock, DEFAULT_ITEM_SUBTYPE, modelResourceLocation);
    }

    private static void registerItem(Item item) {
        ForgeRegistries.ITEMS.register(item);

        ModelResourceLocation modelResourceLocation = new ModelResourceLocation(Objects.requireNonNull((item).getRegistryName()), "inventory");
        ModelLoader.setCustomModelResourceLocation(item, DEFAULT_ITEM_SUBTYPE, modelResourceLocation);
    }

    private static void registerAllSmeltingRecipes() {
        GameRegistry.addSmelting(FlowControlBlock.COPPER_ORE.getBlock(), new ItemStack(FlowControlItem.COPPER_INGOT.getItem()), 1.0F);
    }
}

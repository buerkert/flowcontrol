package de.buerkert.flowcontrol;

import de.buerkert.flowcontrol.creativetab.BuerkertTab;
import de.buerkert.flowcontrol.creativetab.FlowSystemTab;
import de.buerkert.flowcontrol.creativetab.ProductTab;
import de.buerkert.flowcontrol.proxy.IProxy;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import org.apache.logging.log4j.Logger;

@Mod(modid = Main.MODID, name = Main.MODNAME, version = Main.MODVERSION)
public class Main {
    public static final String MODID = "flowcontrol";
    public static final String MODNAME = "Bürkert FlowControl";
    public static final String MODVERSION = "1.0";

    public static final CreativeTabs BUERKERT_TAB = new BuerkertTab("tabBuerkert");
    public static final CreativeTabs FLOW_SYSTEM_TAB = new FlowSystemTab("tabFlowSystem");
    public static final CreativeTabs PRODUCT_TAB = new ProductTab("tabProducts");

    private static Logger logger;

    @SidedProxy(clientSide = "de.buerkert.flowcontrol.proxy.Client", serverSide = "de.buerkert.flowcontrol.proxy.Server")
    public static IProxy proxy;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        logger = event.getModLog();
        proxy.preInit(event);
        Registry.initialize();
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        logger.info(MODNAME + " started");
        proxy.init(event);
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        proxy.postInit(event);
    }

    @Mod.EventHandler
    public void serverStarting(FMLServerStartingEvent event) {
        proxy.serverStarting(event);
    }
}

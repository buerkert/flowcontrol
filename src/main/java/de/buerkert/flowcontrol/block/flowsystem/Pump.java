package de.buerkert.flowcontrol.block.flowsystem;

import de.buerkert.flowcontrol.block.FlowBlock;
import net.minecraft.block.BlockLiquid;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.Objects;
import java.util.Random;

public class Pump extends FlowBlock {

    public Pump() {
        super(Material.IRON);
        setRegistryName("flow_pump");
        setUnlocalizedName("flow_pump");
        setHardness(3.0F);
    }

    @Override
    public boolean canPlaceBlockAt(World worldIn, BlockPos pos) {
        IBlockState stateUp = worldIn.getBlockState(pos.up());
        if((stateUp.getBlock() == Blocks.WATER || stateUp.getBlock() == Blocks.FLOWING_WATER)
                && stateUp.getValue(BlockLiquid.LEVEL) == 0) return false;
        IBlockState[] blockStates = {
                worldIn.getBlockState(pos.down()),
                worldIn.getBlockState(pos.north()),
                worldIn.getBlockState(pos.east()),
                worldIn.getBlockState(pos.south()),
                worldIn.getBlockState(pos.west())
        };
        for(IBlockState blockState : blockStates) {
            if((blockState.getBlock() == Blocks.WATER || blockState.getBlock() == Blocks.FLOWING_WATER)
                    && blockState.getValue(BlockLiquid.LEVEL) == 0) return true;
        }
        return false;
    }

    @Override
    public void randomDisplayTick(IBlockState stateIn, World worldIn, BlockPos pos, Random rand) {
        boolean hasWater = false;
        IBlockState[] blockStates = {
                worldIn.getBlockState(pos.down()),
                worldIn.getBlockState(pos.north()),
                worldIn.getBlockState(pos.east()),
                worldIn.getBlockState(pos.south()),
                worldIn.getBlockState(pos.west())
        };
        for(IBlockState blockState : blockStates) {
            if((blockState.getBlock() == Blocks.WATER || blockState.getBlock() == Blocks.FLOWING_WATER)
                    && blockState.getValue(BlockLiquid.LEVEL) == 0) hasWater = true;
        }
        IBlockState stateUp = worldIn.getBlockState(pos.up());
        if((stateUp.getBlock() == Blocks.WATER || stateUp.getBlock() == Blocks.FLOWING_WATER)
                && stateUp.getValue(BlockLiquid.LEVEL) == 0) hasWater = false;
        if(worldIn.isRemote) {
            if(!hasWater) {
                worldIn.destroyBlock(pos, true);
                Objects.requireNonNull(Minecraft.getMinecraft().getIntegratedServer()).getEntityWorld().destroyBlock(pos, true);
            }
        }
    }
}

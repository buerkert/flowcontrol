package de.buerkert.flowcontrol.block.flowsystem;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import de.buerkert.flowcontrol.block.FacingBlock;
import de.buerkert.flowcontrol.block.FlowBlock;
import de.buerkert.flowcontrol.block.FlowControlBlock;
import de.buerkert.flowcontrol.flowsystem.AttachPosition;
import de.buerkert.flowcontrol.flowsystem.FlowWorld;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.properties.PropertyInteger;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.*;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.Set;

public class Tube extends FlowBlock {
    public static final PropertyEnum<AttachPosition> NORTH = PropertyEnum.<AttachPosition>create("north", AttachPosition.class);
    public static final PropertyEnum<AttachPosition> EAST = PropertyEnum.<AttachPosition>create("east", AttachPosition.class);
    public static final PropertyEnum<AttachPosition> SOUTH = PropertyEnum.<AttachPosition>create("south", AttachPosition.class);
    public static final PropertyEnum<AttachPosition> WEST = PropertyEnum.<AttachPosition>create("west", AttachPosition.class);

    public static final PropertyInteger STRENGTH = PropertyInteger.create("strength", 0, 15);
    public static final PropertyBool PUMP_DOWN = PropertyBool.create("pump");

    private static final AxisAlignedBB COLLISION_BOX = new AxisAlignedBB(0, 0, 0, 1, 0.250, 1);
    private static final AxisAlignedBB[] TUBE_AABB = new AxisAlignedBB[] {new AxisAlignedBB(0.25, 0.0, 0.25, 0.75, 0.375, 0.75), new AxisAlignedBB(0.25, 0, 0.25, 0.75, 0.375, 1), new AxisAlignedBB(0, 0, 0.25, 0.75, 0.375, 0.75), new AxisAlignedBB(0, 0, 0.25, 0.75, 0.375, 1), new AxisAlignedBB(0.25, 0, 0, 0.75, 0.375, 0.75), new AxisAlignedBB(0.25, 0, 0, 0.75, 0.375, 1), new AxisAlignedBB(0, 0, 0, 0.75, 0.375, 0.75), new AxisAlignedBB(0, 0, 0, 0.75, 0.375, 1), new AxisAlignedBB(0.25, 0, 0.25, 1, 0.375, 0.75), new AxisAlignedBB(0.25, 0, 0.25, 1, 0.375, 1), new AxisAlignedBB(0, 0, 0.25, 1, 0.375, 0.75), new AxisAlignedBB(0, 0, 0.25, 1, 0.375, 1), new AxisAlignedBB(0.25, 0, 0, 1, 0.375, 0.75), new AxisAlignedBB(0.25, 0, 0, 1, 0.375, 1), new AxisAlignedBB(0, 0, 0, 1, 0.375, 0.75), new AxisAlignedBB(0, 0, 0, 1, 0.375, 1)};
    private boolean canProvideWater = true;

    private final Set<BlockPos> blocksNeedingUpdate = Sets.<BlockPos>newHashSet();

    public Tube() {
        super(Material.CIRCUITS);
        setRegistryName("flow_tube");
        setUnlocalizedName("flow_tube");
        setDefaultState(this.blockState.getBaseState().withProperty(NORTH, AttachPosition.NONE)
            .withProperty(EAST, AttachPosition.SIDE)
            .withProperty(SOUTH, AttachPosition.NONE)
            .withProperty(WEST, AttachPosition.SIDE)
            .withProperty(STRENGTH, Integer.valueOf(0))
            .withProperty(PUMP_DOWN, Boolean.valueOf(false)));
    }

    @Override
    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos) {
        return TUBE_AABB[getAABBIndex(state.getActualState(source, pos))];
    }

    private static int getAABBIndex(IBlockState state) {
        int i = 0;
        boolean flagNorth = state.getValue(NORTH) != AttachPosition.NONE;
        boolean flagEast = state.getValue(EAST) != AttachPosition.NONE;
        boolean flagSouth = state.getValue(SOUTH) != AttachPosition.NONE;
        boolean flagWest = state.getValue(WEST) != AttachPosition.NONE;
        if(flagNorth || flagSouth && !flagNorth && !flagEast && !flagWest) {
            i |= 1 << EnumFacing.NORTH.getHorizontalIndex();
        }
        if(flagEast || flagWest && !flagNorth && !flagEast && !flagSouth) {
            i |= 1 << EnumFacing.EAST.getHorizontalIndex();
        }
        if(flagSouth || flagNorth && !flagEast && !flagSouth && !flagWest) {
            i |= 1 << EnumFacing.SOUTH.getHorizontalIndex();
        }
        if(flagWest || flagEast && !flagNorth && !flagSouth && !flagWest) {
            i |= 1 << EnumFacing.WEST.getHorizontalIndex();
        }
        return i;
    }

    @Override
    public IBlockState getActualState(IBlockState state, IBlockAccess worldIn, BlockPos pos) {
        state = state.withProperty(WEST, this.getAttachPosition(worldIn, pos, EnumFacing.WEST));
        state = state.withProperty(SOUTH, this.getAttachPosition(worldIn, pos, EnumFacing.SOUTH));
        state = state.withProperty(EAST, this.getAttachPosition(worldIn, pos, EnumFacing.EAST));
        state = state.withProperty(NORTH, this.getAttachPosition(worldIn, pos, EnumFacing.NORTH));
        state = state.withProperty(PUMP_DOWN, worldIn.getBlockState(pos.down()).getBlock().equals(FlowControlBlock.PUMP.getBlock()));
        return state;
    }

    private AttachPosition getAttachPosition(IBlockAccess worldIn, BlockPos pos, EnumFacing direction) {
        BlockPos blockPos = pos.offset(direction);
        IBlockState iBlockState = worldIn.getBlockState(pos.offset(direction));
        if(!canConnectTo(worldIn.getBlockState(blockPos), direction, worldIn, blockPos) && (iBlockState.isNormalCube() || !canConnectUpwardsTo(worldIn, blockPos.down()))) {
            IBlockState iBlockState1 = worldIn.getBlockState(pos.up());
            if(!iBlockState1.isNormalCube()) {
                boolean flag = worldIn.getBlockState(blockPos).isSideSolid(worldIn, blockPos, EnumFacing.UP) || worldIn.getBlockState(blockPos).getBlock() == Blocks.GLOWSTONE;
                if(flag && canConnectUpwardsTo(worldIn, blockPos.up())) {
                    if(iBlockState.isBlockNormalCube()) {
                        return AttachPosition.UP;
                    }
                    return AttachPosition.SIDE;
                }
            }
            return AttachPosition.NONE;
        } else {
            return AttachPosition.SIDE;
        }
    }

    @Nullable
    public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, IBlockAccess worldIn, BlockPos pos) {
        return TUBE_AABB[getAABBIndex(blockState.getActualState(worldIn, pos))];
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullBlock(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean canPlaceBlockAt(World worldIn, BlockPos pos) {
        IBlockState downState = worldIn.getBlockState(pos.down());
        return (downState.isTopSolid() || downState.getBlockFaceShape(worldIn, pos.down(), EnumFacing.UP) == BlockFaceShape.SOLID || worldIn.getBlockState(pos.down()).getBlock() == Blocks.GLOWSTONE) && !Objects.requireNonNull(downState.getBlock().getRegistryName()).toString().contains("product");
    }

    private IBlockState updateSurroundingTube(World worldIn, BlockPos pos, IBlockState state) {
        state = this.calculateCurrentChanges(worldIn, pos, pos, state);
        List<BlockPos> list = Lists.newArrayList(this.blocksNeedingUpdate);
        this.blocksNeedingUpdate.clear();
        for(BlockPos blockPos : list) {
            worldIn.notifyNeighborsOfStateChange(blockPos, this, false);
        }
        return state;
    }

    private IBlockState calculateCurrentChanges(World worldIn, BlockPos pos1, BlockPos pos2, IBlockState state) {
        IBlockState iBlockState = state;
        int i = (Integer)state.getValue(STRENGTH);
        int j = 0;
        j = this.getMaxCurrentStrength(worldIn, pos2, j);
        this.canProvideWater = false;
        int k = FlowWorld.isBlockIndirectGettingWatered(worldIn, pos1);
        this.canProvideWater = true;
        if(k > 0 && k > j - 1) {
            j = k;
        }
        int l = 0;
        for(EnumFacing enumFacing : EnumFacing.Plane.HORIZONTAL) {
            BlockPos blockPos = pos1.offset(enumFacing);
            boolean flag = blockPos.getX() != pos2.getX() || blockPos.getZ() != pos2.getZ();
            if(flag) {
                l = this.getMaxCurrentStrength(worldIn, blockPos, l);
            }

            if(worldIn.getBlockState(blockPos).isNormalCube() && !worldIn.getBlockState(pos1.up()).isNormalCube()) {
                if(flag && pos1.getY() >= pos2.getY()) {
                    l = this.getMaxCurrentStrength(worldIn, blockPos.up(), l);
                }
            } else if(!worldIn.getBlockState(blockPos).isNormalCube() && flag && pos1.getY() <= pos2.getY()) {
                l = this.getMaxCurrentStrength(worldIn, blockPos.down(), l);
            }
        }
        if(l > j) {
            j = l - 1;
        } else if(j > 0) {
            --j;
        } else {
            j = 0;
        }

        if(k > j -1 ) {
            j = k;
        }

        if(i != j) {
            state = state.withProperty(STRENGTH, j);
            if(worldIn.getBlockState(pos1) == iBlockState) {
                worldIn.setBlockState(pos1, state, 2);
            }
            this.blocksNeedingUpdate.add(pos1);
            for(EnumFacing enumFacing : EnumFacing.values()) {
                this.blocksNeedingUpdate.add(pos1.offset(enumFacing));
            }
        }
        return state;
    }

    private void notifyTubeNeightborsOfStateChange(World worldIn, BlockPos pos) {
        if(worldIn.getBlockState(pos).getBlock() == this) {
            worldIn.notifyNeighborsOfStateChange(pos, this, false);
            for(EnumFacing enumFacing : EnumFacing.values()) {
                worldIn.notifyNeighborsOfStateChange(pos.offset(enumFacing), this, false);
            }
        }
    }

    @Override
    public void onBlockAdded(World worldIn, BlockPos pos, IBlockState state) {
        if(!worldIn.isRemote) {
            this.updateSurroundingTube(worldIn, pos, state);
            for(EnumFacing enumFacing : EnumFacing.values()) {
                worldIn.notifyNeighborsOfStateChange(pos.offset(enumFacing), this, false);
            }
            notifyTubeIfNeeded(worldIn, pos);
        }
    }

    private void notifyTubeIfNeeded(World worldIn, BlockPos pos) {
        for(EnumFacing enumFacing : EnumFacing.values()) {
            this.notifyTubeNeightborsOfStateChange(worldIn, pos.offset(enumFacing));
        }
        for(EnumFacing enumFacing : EnumFacing.values()) {
            BlockPos blockPos = pos.offset(enumFacing);
            if(worldIn.getBlockState(blockPos).isNormalCube()) {
                this.notifyTubeNeightborsOfStateChange(worldIn, blockPos.up());
            } else {
                this.notifyTubeNeightborsOfStateChange(worldIn, blockPos.down());
            }
        }
    }

    @Override
    public void breakBlock(World worldIn, BlockPos pos, IBlockState state) {
        super.breakBlock(worldIn, pos, state);
        if(!worldIn.isRemote) {
            for(EnumFacing enumFacing : EnumFacing.values()) {
                worldIn.notifyNeighborsOfStateChange(pos.offset(enumFacing), this, false);
            }
            this.updateSurroundingTube(worldIn, pos, state);
            notifyTubeIfNeeded(worldIn, pos);
        }
    }


    private int getMaxCurrentStrength(World worldIn, BlockPos pos, int strength) {
        if(worldIn.getBlockState(pos).getBlock() != this) {
            return strength;
        } else {
            int i = (Integer)worldIn.getBlockState(pos).getValue(STRENGTH);
            return i > strength ? i : strength;
        }
    }

    @Override
    public void neighborChanged(IBlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos) {
        if(!worldIn.isRemote) {
            if(this.canPlaceBlockAt(worldIn, pos)) {
                updateSurroundingTube(worldIn, pos, state);
            } else {
                this.dropBlockAsItem(worldIn, pos, state, 0);
                worldIn.setBlockToAir(pos);
            }
        }
    }

    public static boolean canConnectUpwardsTo(IBlockAccess worldIn, BlockPos pos) {
        IBlockState state = worldIn.getBlockState(pos);
        if(state.getBlock().equals(FlowControlBlock.TUBE.getBlock()) || state.getBlock().equals(FlowControlBlock.PUMP.getBlock()) || state.getBlock().equals(FlowControlBlock.OUTLET.getBlock())) {
            return canConnectTo(worldIn.getBlockState(pos), null, worldIn, pos);
        }
        return false;
    }

    public static boolean canConnectTo(IBlockState blockState, @Nullable EnumFacing side, IBlockAccess worldIn, BlockPos pos) {
        Block block = blockState.getBlock();
        if(block.equals(FlowControlBlock.TUBE.getBlock()) || block.equals(FlowControlBlock.PUMP.getBlock()) || block.equals(FlowControlBlock.OUTLET.getBlock())) {
            return true;
        }
        if(block.equals(FlowControlBlock.REPEATER.getBlock()) && (blockState.getValue(Repeater.FACING) == side || blockState.getValue(Repeater.FACING) == side.getOpposite())) {
            return true;
        }
        if(Objects.requireNonNull(block.getRegistryName()).toString().contains("product") && ((blockState.getValue(FacingBlock.FACING) == side) || (blockState.getValue(FacingBlock.FACING) == side.getOpposite()))) {
            return true;
        }
        return false;
    }


    @SideOnly(Side.CLIENT)
    public void randomDisplayTick(IBlockState state, World worldIn, BlockPos pos, Random rand) {
        int i = (Integer)state.getValue(STRENGTH);
        if(i != 0) {
            double d0 = (double)pos.getX() + 0.5D + ((double)rand.nextFloat() - 0.5D) * 0.2D;
            double d1 = (double)((float)pos.getX() + 0.0625F);
            double d2 = (double)pos.getZ() + 0.5D + ((double)rand.nextFloat() - 0.5D) * 0.2D;
            float f = (float)i / 15.0F;
            float f0 = f * 0.6F + 0.4F;
            float f1 = Math.max(0.0F, f * f * 0.7F - 0.5F);
            float f2 = Math.max(0.0F, f * f * 0.6F - 0.7F);
            worldIn.spawnParticle(EnumParticleTypes.DRIP_WATER, d0, d1, d2, (double)f0, (double)f1, (double)f2);
        }
    }


    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune) {
        return Item.getItemFromBlock(this);
    }

    @Override
    public ItemStack getItem(World worldIn, BlockPos pos, IBlockState state) {
        return new ItemStack(Item.getItemFromBlock(this));
    }

    @Override
    public IBlockState getStateFromMeta(int meta) {
        return this.getDefaultState().withProperty(STRENGTH, meta);
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        return (Integer)state.getValue(STRENGTH);
    }


    @Override
    public IBlockState withRotation(IBlockState state, Rotation rot) {
        switch (rot) {
            case CLOCKWISE_180:
                return state.withProperty(NORTH, state.getValue(SOUTH)).withProperty(EAST, state.getValue(WEST)).withProperty(SOUTH, state.getValue(NORTH)).withProperty(WEST, state.getValue(EAST));
            case COUNTERCLOCKWISE_90:
                return state.withProperty(NORTH, state.getValue(EAST)).withProperty(EAST, state.getValue(SOUTH)).withProperty(SOUTH, state.getValue(WEST)).withProperty(WEST, state.getValue(NORTH));
            case CLOCKWISE_90:
                return state.withProperty(NORTH, state.getValue(WEST)).withProperty(EAST, state.getValue(NORTH)).withProperty(SOUTH, state.getValue(EAST)).withProperty(WEST, state.getValue(SOUTH));
            default:
                return state;
        }
    }

    @Override
    public IBlockState withMirror(IBlockState state, Mirror mirrorIn) {
        switch (mirrorIn) {
            case LEFT_RIGHT:
                return state.withProperty(NORTH, state.getValue(SOUTH)).withProperty(SOUTH, state.getValue(NORTH));
            case FRONT_BACK:
                return state.withProperty(EAST, state.getValue(WEST)).withProperty(WEST, state.getValue(EAST));
            default:
                return super.withMirror(state, mirrorIn);
        }
    }


    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer(this, new IProperty[]{NORTH, EAST, SOUTH, WEST, STRENGTH, PUMP_DOWN});
    }

    @Override
    public BlockRenderLayer getBlockLayer() {
        return BlockRenderLayer.CUTOUT;
    }

    @Override
    public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing face) {
        return BlockFaceShape.UNDEFINED;
    }

    @Override
    public boolean canProvideWater(IBlockState blockState) {
        return this.canProvideWater;
    }
}

package de.buerkert.flowcontrol.block.flowsystem;

import de.buerkert.flowcontrol.block.FlowBlock;
import de.buerkert.flowcontrol.block.FlowControlBlock;
import de.buerkert.flowcontrol.flowsystem.FlowWorld;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.properties.PropertyDirection;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.*;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.event.ForgeEventFactory;

import javax.annotation.Nullable;
import java.util.EnumSet;
import java.util.Random;

public class Repeater extends FlowBlock {

    public static final PropertyDirection FACING = PropertyDirection.create("facing", EnumFacing.Plane.HORIZONTAL);
    public static final PropertyBool POWER = PropertyBool.create("power");
    public static final PropertyBool EXTENSION = PropertyBool.create("extension");

    public static final AxisAlignedBB REPEATER_AABB = new AxisAlignedBB(0, 0, 0, 1, 0.4375, 1);
    private boolean isRepeaterPowered = false;

    public Repeater() {
        super(Material.CIRCUITS);
        setRegistryName("flow_repeater");
        setUnlocalizedName("flow_repeater");
        setHardness(3.0F);
        this.setDefaultState(this.blockState.getBaseState().withProperty(FACING, EnumFacing.NORTH).withProperty(POWER, Boolean.valueOf(false)).withProperty(EXTENSION, Boolean.valueOf(false)));
    }

    @Override
    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos) {
        return REPEATER_AABB;
    }

    @Nullable
    public AxisAlignedBB getCollisionBoundingBox(IBlockState state, IBlockAccess worldIn, BlockPos pos) {
        return REPEATER_AABB;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullBlock(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack itemStack) {
        if(this.shouldBePowered(worldIn, pos, state)) {
            worldIn.scheduleUpdate(pos, this, 1);
        }
        this.updateState(worldIn, pos, state);
    }

    @Override
    public boolean canPlaceBlockAt(World worldIn, BlockPos pos) {
        IBlockState downState = worldIn.getBlockState(pos.down());
        return (downState.isTopSolid() || downState.getBlockFaceShape(worldIn, pos.down(), EnumFacing.UP) == BlockFaceShape.SOLID) ? super.canPlaceBlockAt(worldIn, pos) : false;
    }

    public boolean canBlockStay(World worldIn, BlockPos pos) {
        IBlockState downState = worldIn.getBlockState(pos.down());
        return (downState.isTopSolid() || downState.getBlockFaceShape(worldIn, pos.down(), EnumFacing.UP) == BlockFaceShape.SOLID);
    }

    @Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        worldIn.setBlockState(pos, state.cycleProperty(EXTENSION), 3);
        return true;
    }

    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune) {
        return Item.getItemFromBlock(this);
    }

    @Override
    public ItemStack getItem(World worldIn, BlockPos pos, IBlockState state) {
        return new ItemStack(Item.getItemFromBlock(this));
    }

    @Override
    public void neighborChanged(IBlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos) {
        if(this.canBlockStay(worldIn, pos)) {
            this.updateState(worldIn, pos, state);
        } else {
            this.dropBlockAsItem(worldIn, pos, state, 0);
            worldIn.setBlockToAir(pos);

            for(EnumFacing enumFacing : EnumFacing.values()) {
                worldIn.notifyNeighborsOfStateChange(pos.offset(enumFacing), this, false);
            }
        }
    }

    public void updateState(World worldIn, BlockPos pos, IBlockState state) {
        boolean flag = this.shouldBePowered(worldIn, pos, state);
        this.isRepeaterPowered = flag;
        if(!worldIn.isBlockTickPending(pos, this)) {
            int i = -1;
            if(this.isFacingTowardsRepeater(worldIn, pos, state)) {
                i = -3;
            } else if(this.isRepeaterPowered) {
                i = -2;
            }
            worldIn.updateBlockTick(pos, this, 3, i);
            worldIn.setBlockState(pos, state.withProperty(POWER, this.isRepeaterPowered), 3);
        }
    }

    public boolean shouldBePowered(World worldIn, BlockPos pos, IBlockState state) {
        return this.calculateInputStrength(worldIn, pos, state) > 0;
    }

    public int calculateInputStrength(World worldIn, BlockPos pos, IBlockState state) {
        EnumFacing enumFacing = (EnumFacing)state.getValue(FACING);
        BlockPos blockPos = pos.offset(enumFacing);
        int i = FlowWorld.getWaterPower(worldIn, blockPos, enumFacing);
        if(i >= 15) return i;
        IBlockState iBlockState = worldIn.getBlockState(blockPos);
        return Math.max(i, iBlockState.getBlock().equals(FlowControlBlock.TUBE.getBlock()) ? ((Integer)iBlockState.getValue(Tube.STRENGTH)).intValue() : 0);
    }

    public boolean canProvideWater(IBlockState state) {
        return true;
    }

    public void notifyNeighbors(World worldIn, BlockPos pos, IBlockState state) {
        EnumFacing enumFacing = (EnumFacing)state.getValue(FACING);
        BlockPos blockPos = pos.offset(enumFacing.getOpposite());
        if(ForgeEventFactory.onNeighborNotify(worldIn, pos, worldIn.getBlockState(pos), EnumSet.of(enumFacing.getOpposite()), false).isCancelable()) return;
        worldIn.neighborChanged(blockPos, this, pos);
        worldIn.notifyNeighborsOfStateExcept(blockPos, this, enumFacing);
    }

    @Override
    public void onBlockDestroyedByPlayer(World worldIn, BlockPos blockPos, IBlockState state) {
        if(this.isRepeaterPowered) {
            for(EnumFacing enumFacing : EnumFacing.values()) {
                worldIn.notifyNeighborsOfStateChange(blockPos.offset(enumFacing), this, false);
            }
        }
        super.onBlockDestroyedByPlayer(worldIn, blockPos, state);
    }

    @Override
    public IBlockState getStateForPlacement(World worldIn, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer) {
        return this.getDefaultState().withProperty(FACING, placer.getHorizontalFacing().getOpposite());
    }

    @Override
    public void breakBlock(World worldIn, BlockPos pos, IBlockState state) {
        super.breakBlock(worldIn, pos, state);
        this.notifyNeighbors(worldIn, pos, state);
    }

    public boolean isWaterRepeater(IBlockState state) {
        return this.isSameWaterRepeater(state);
    }

    public boolean isSameWaterRepeater(IBlockState state) {
        Block block = state.getBlock();
        return block == this.getWaterRepeater(state);
    }

    public IBlockState getWaterRepeater(IBlockState state) {
        if(state.getBlock().equals(FlowControlBlock.REPEATER.getBlock())) {
            EnumFacing enumFacing = ((EnumFacing) state.getValue(FACING).getOpposite());
            Boolean power = (Boolean) state.getValue(POWER);
            Boolean expansion = (Boolean) state.getValue(EXTENSION);
            return this.getDefaultState().withProperty(FACING, enumFacing).withProperty(POWER, power).withProperty(EXTENSION, expansion);
        }
        return state;
    }

    public boolean isFacingTowardsRepeater(World worldIn, BlockPos pos, IBlockState state) {
        EnumFacing enumFacing = ((EnumFacing)state.getValue(FACING).getOpposite());
        BlockPos blockPos = pos.offset(enumFacing);
        if(isWaterRepeater(worldIn.getBlockState(blockPos))) {
            return worldIn.getBlockState(blockPos).getValue(FACING) != enumFacing;
        }
        return false;
    }

    @Override
    public IBlockState withRotation(IBlockState state, Rotation rot) {
        return state.withProperty(FACING, rot.rotate((EnumFacing)state.getValue(FACING)));
    }

    @Override
    public IBlockState withMirror(IBlockState state, Mirror mirrorIn) {
        return state.withRotation(mirrorIn.toRotation((EnumFacing)state.getValue(FACING)));
    }

    @Override
    public IBlockState getStateFromMeta(int meta) {
        int facing = bitExtract(meta, 2, 1);
        boolean power = (1 == bitExtract(meta, 1, 3));
        boolean expansion = (1 == bitExtract(meta, 1, 4));
        return this.getDefaultState().withProperty(FACING, EnumFacing.getHorizontal(facing)).withProperty(POWER, Boolean.valueOf(power)).withProperty(EXTENSION, Boolean.valueOf(expansion));
    }

    static int bitExtract(int number, int k, int p) {
        return (((1 << k) - 1) & (number >> (p - 1)));
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        int i = 0;
        i = i | ((EnumFacing)state.getValue(FACING)).getHorizontalIndex();
        int j = (Boolean)state.getValue(POWER) ? 1 : 0;
        i = i | j << 2;
        int k = (Boolean)state.getValue(EXTENSION) ? 1 : 0;
        i = i | k << 3;
        return i;
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer(this, new IProperty[]{FACING, POWER, EXTENSION});
    }

    @Override
    public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing facing) {
        return facing == EnumFacing.DOWN ? BlockFaceShape.SOLID : BlockFaceShape.UNDEFINED;
    }

    @Override
    public BlockRenderLayer getBlockLayer() {
        return BlockRenderLayer.CUTOUT;
    }
}

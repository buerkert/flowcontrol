package de.buerkert.flowcontrol.block.flowsystem;

import com.google.common.collect.Lists;
import de.buerkert.flowcontrol.block.FlowBlock;
import de.buerkert.flowcontrol.flowsystem.FlowWorld;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Objects;
import java.util.Random;

public class Outlet extends FlowBlock {

    public static final PropertyBool POWER = PropertyBool.create("power");
    private boolean isPowered = false;

    private static final AxisAlignedBB CUBE0 = new AxisAlignedBB(0, 0, 0, 1, 0.062, 0.062);
    private static final AxisAlignedBB CUBE1 = new AxisAlignedBB(0, 0, 0.938, 1, 0.062, 1);
    private static final AxisAlignedBB CUBE2 = new AxisAlignedBB(0, 0, 0.062, 0.062, 0.062, 0.938);
    private static final AxisAlignedBB CUBE3 = new AxisAlignedBB(0.938, 0, 0.062, 1, 0.062, 0.938);
    private static final AxisAlignedBB CUBE4 = new AxisAlignedBB(0, 0.062, 0, 1, 0.125, 1);
    private static final AxisAlignedBB CUBE5 = new AxisAlignedBB(0, 0.125, 0.25, 1, 0.25, 0.75);
    private static final AxisAlignedBB CUBE6 = new AxisAlignedBB(0.25, 0.125, 0, 0.75, 0.25, 0.25);
    private static final AxisAlignedBB CUBE7 = new AxisAlignedBB(0.25, 0.125, 0.75, 0.75, 0.25, 1);
    private static final AxisAlignedBB CUBE8 = new AxisAlignedBB(0.312, 0.25, 0.688, 0.688, 0.438, 1);
    private static final AxisAlignedBB CUBE9 = new AxisAlignedBB(0.312, 0.25, 0, 0.688, 0.438, 0.312);
    private static final AxisAlignedBB CUBE10 = new AxisAlignedBB(0, 0.25, 0.312, 1, 0.438, 0.688);
    private static final AxisAlignedBB CUBE11 = new AxisAlignedBB(0, 0.438, 0.375, 1, 0.5, 0.625);
    private static final AxisAlignedBB CUBE12 = new AxisAlignedBB(0.375, 0.438, 0, 0.625, 0.5, 0.375);
    private static final AxisAlignedBB CUBE13 = new AxisAlignedBB(0.375, 0.438, 0.625, 0.625, 0.5, 1);

    private static final List<AxisAlignedBB> COLLISION_BOXES = Lists.newArrayList(CUBE0, CUBE1, CUBE2, CUBE3, CUBE4, CUBE5, CUBE6, CUBE7, CUBE8, CUBE9, CUBE10, CUBE11, CUBE12, CUBE13);
    private static final AxisAlignedBB BOUNDING_BOX = new AxisAlignedBB(0, 0, 0, 1, 0.5, 1);

    public Outlet() {
        super(Material.IRON);
        setRegistryName("flow_outlet");
        setUnlocalizedName("flow_outlet");
        setHardness(3.0F);
        this.isPowered = false;
        this.setDefaultState(this.blockState.getBaseState().withProperty(POWER, Boolean.valueOf(isPowered)));
    }

    @Override
    public boolean isFullBlock(IBlockState state) {
        return fullBlock;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return fullBlock;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return fullBlock;
    }

    @Override
    public BlockRenderLayer getBlockLayer() {
        return BlockRenderLayer.CUTOUT_MIPPED;
    }

    @Override
    public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack) {
        super.onBlockPlacedBy(worldIn, pos, state, placer, stack);
        this.updateState(worldIn, pos, state);
    }

    @Override
    public boolean canPlaceBlockAt(World worldIn, BlockPos pos) {
        Block blockDown = worldIn.getBlockState(pos.down()).getBlock();
        return blockDown.equals(Blocks.AIR) || blockDown.equals(Blocks.WATER) || blockDown.equals(Blocks.FLOWING_WATER);
    }

    @Override
    public void randomDisplayTick(IBlockState stateIn, World worldIn, BlockPos pos, Random rand) {
        super.randomDisplayTick(stateIn, worldIn, pos, rand);
        this.updateState(worldIn, pos, stateIn);
        Block blockDown = worldIn.getBlockState(pos.down()).getBlock();

        if(blockDown.equals(Blocks.AIR) || blockDown.equals(Blocks.FLOWING_WATER)) {
            if(isPowered) {
                if (worldIn.isRemote) {
                    worldIn.setBlockState(pos.down(), (IBlockState) Blocks.WATER.getDefaultState(), 3);
                    Objects.requireNonNull(Minecraft.getMinecraft().getIntegratedServer()).getEntityWorld().setBlockState(pos.down(), (IBlockState) Blocks.WATER.getDefaultState(), 3);
                }
            }
            return;
        } else if(blockDown.equals(Blocks.WATER)) {
            if (!isPowered) {
                if (worldIn.isRemote) {
                    worldIn.setBlockToAir(pos.down());
                    Objects.requireNonNull(Minecraft.getMinecraft().getIntegratedServer()).getEntityWorld().setBlockToAir(pos.down());
                }
            }
            return;
        }
        this.dropBlockAsItem(worldIn, pos, stateIn, 0);
        if(worldIn.isRemote) {
            worldIn.setBlockToAir(pos);
            Objects.requireNonNull(Minecraft.getMinecraft().getIntegratedServer()).getEntityWorld().setBlockToAir(pos);
        }
    }

    private void updateState(World worldIn, BlockPos pos, IBlockState state) {
        isPowered = this.shouldBePowered(worldIn, pos, state);
        worldIn.updateBlockTick(pos, this, 3, -1);
        worldIn.setBlockState(pos, state.withProperty(POWER, isPowered), 3);
    }

    public boolean shouldBePowered(World worldIn, BlockPos pos, IBlockState state) {
        return this.calculateInputStrength(worldIn, pos, state) > 0;
    }

    public int calculateInputStrength(World worldIn, BlockPos pos, IBlockState state) {
        int strength = 0;
        for(EnumFacing enumFacing : EnumFacing.values()) {
            BlockPos blockPos = pos.offset(enumFacing);
            int i = FlowWorld.getWaterPower(worldIn, blockPos, enumFacing);
            if(strength < i) strength = i;
        }
        return strength;
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer(this, new IProperty[]{POWER});
    }


    static int bitExtracted(int number, int k, int p)
    {
        return (((1 << k) - 1) & (number >> (p - 1)));
    }

    @Override
    public IBlockState getStateFromMeta(int meta) {
        boolean power = (1 == bitExtracted(meta, 1, 1));
        return this.getDefaultState().withProperty(POWER, Boolean.valueOf(power));
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        int i = 0;
        int j = (Boolean)state.getValue(POWER) ? 1 : 0;
        i = i | j;
        return i;
    }

    @Override
    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
    {
        return BOUNDING_BOX;
    }

    @Override
    public void addCollisionBoxToList(IBlockState state, World world, BlockPos pos, AxisAlignedBB entityBox, List<AxisAlignedBB> collidingBoxes, @Nullable Entity entity, boolean isActualState)
    {
        entityBox = entityBox.offset(-pos.getX(), -pos.getY(), -pos.getZ());
        for (AxisAlignedBB box : COLLISION_BOXES)
        {
            if (entityBox.intersects(box))
                collidingBoxes.add(box.offset(pos));
        }
    }
}

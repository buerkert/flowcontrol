package de.buerkert.flowcontrol.block;

import de.buerkert.flowcontrol.Main;
import de.buerkert.flowcontrol.gui.ProductGui;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyDirection;
import net.minecraft.block.properties.PropertyInteger;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class ProductBlock extends FacingBlock {

    private String[] properties;

    public static final PropertyInteger FLOW = PropertyInteger.create("flow", 0, 3);
    public static final PropertyDirection FACING = PropertyDirection.create("facing", EnumFacing.Plane.HORIZONTAL);
    public String websiteI18n;

    public ProductBlock(String name, String websiteI18n, AxisAlignedBB boundingBox, AxisAlignedBB... collisionBoxes) {
        super(name, Material.IRON, Main.PRODUCT_TAB, 3.0F, boundingBox, collisionBoxes);
        this.setDefaultState(this.blockState.getBaseState().withProperty(FLOW, Integer.valueOf(0)).withProperty(FACING, EnumFacing.NORTH));
        this.websiteI18n = websiteI18n;
    }

    public void setProperties(String... properties) {
        if(properties.length % 2 != 0) {
            this.properties = new String[]{"error", "Please enter an even amount of property parameter"};
            return;
        }
        this.properties = properties;
    }

    public String[] getProperties() {
        return properties;
    }

    static int bitExtracted(int number, int k, int p) {
        return (((1 << k) - 1) & (number >> (p - 1)));
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        int i = 0;
        int j = (Integer)state.getValue(FLOW);
        i = i | ((EnumFacing)state.getValue(FACING)).getHorizontalIndex();
        i = i | j << 2;
        return i;
    }

    @Override
    public IBlockState getStateFromMeta(int meta) {
        int facing = bitExtracted(meta, 2, 1);
        int flow = bitExtracted(meta, 2, 3);
        return this.getDefaultState().withProperty(FACING, EnumFacing.getHorizontal(facing)).withProperty(FLOW, Integer.valueOf(flow));
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer(this, new IProperty[]{FACING, FLOW});
    }

    public void setFlow(int flow, World worldIn, BlockPos pos, IBlockState state) {
        worldIn.setBlockState(pos, state.withProperty(FLOW, flow), 3);
    }

    public int getFlow(IBlockState state) {
        return (Integer)state.getValue(FLOW);
    }

    @Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        Minecraft.getMinecraft().displayGuiScreen(new ProductGui(this, worldIn, pos, state, playerIn));
        return super.onBlockActivated(worldIn, pos, state, playerIn, hand, facing, hitX, hitY, hitZ);
    }
}

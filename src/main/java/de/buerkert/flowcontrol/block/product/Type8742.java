package de.buerkert.flowcontrol.block.product;

import de.buerkert.flowcontrol.block.ProductBlock;
import net.minecraft.util.math.AxisAlignedBB;

public class Type8742 extends ProductBlock {
    private static final AxisAlignedBB CUBE0 = new AxisAlignedBB(0.312, 0, 0, 0.688, 0.062, 1);
    private static final AxisAlignedBB CUBE1 = new AxisAlignedBB(0.312, 0.062, 0, 0.438, 0.188, 1);
    private static final AxisAlignedBB CUBE2 = new AxisAlignedBB(0.562, 0.062, 0, 0.688, 0.188, 1);
    private static final AxisAlignedBB CUBE3 = new AxisAlignedBB(0.312, 0.188, 0, 0.688, 0.25, 1);
    private static final AxisAlignedBB CUBE4 = new AxisAlignedBB(0.312, 0.25, 0.125, 0.688, 0.5, 1);
    private static final AxisAlignedBB CUBE5 = new AxisAlignedBB(0.312, 0.5, 0.5, 0.688, 0.75, 1);
    private static final AxisAlignedBB CUBE6 = new AxisAlignedBB(0.25, 0.5, 0, 0.75, 0.812, 0.625);
    private static final AxisAlignedBB CUBE7 = new AxisAlignedBB(0.25, 0.688, 0.5, 0.75, 0.812, 0.688);
    private static final AxisAlignedBB CUBE8 = new AxisAlignedBB(0.375, 0.25, 0, 0.625, 0.5, 0.125);

    private static final AxisAlignedBB BOUNDING_BOX = new AxisAlignedBB(0.25, 0, 0, 0.75, 0.812, 1);

    public Type8742() {
        super("product_type_8742","uri.type8742.name", BOUNDING_BOX, CUBE0, CUBE1, CUBE2, CUBE3, CUBE4, CUBE5, CUBE6, CUBE7, CUBE8);
        setProperties("mass", "ca. 950g",
                "sealing", "FKM / EPDM",
                "protection", "IP65 & IP 67",
                "operatingvoltage", "24V DC");
    }
}

package de.buerkert.flowcontrol.block.product;

import de.buerkert.flowcontrol.block.ProductBlock;
import net.minecraft.util.math.AxisAlignedBB;

public class Type8719 extends ProductBlock {
    private static final AxisAlignedBB CUBE0 = new AxisAlignedBB(0.312, 0, 0, 0.688, 0.062, 1);
    private static final AxisAlignedBB CUBE1 = new AxisAlignedBB(0.312, 0.062, 0, 0.438, 0.188, 1);
    private static final AxisAlignedBB CUBE2 = new AxisAlignedBB(0.562, 0.062, 0, 0.688, 0.188, 1);
    private static final AxisAlignedBB CUBE3 = new AxisAlignedBB(0.312, 0.188, 0, 0.688, 0.25, 1);
    private static final AxisAlignedBB CUBE4 = new AxisAlignedBB(0.312, 0.25, 0.062, 0.688, 0.75, 0.938);
    private static final AxisAlignedBB CUBE5 = new AxisAlignedBB(0.312, 0.938, 0.188, 0.688, 1, 0.938);
    private static final AxisAlignedBB CUBE6 = new AxisAlignedBB(0.438, 0.5, 0, 0.5, 0.625, 0.062);
    private static final AxisAlignedBB CUBE7 = new AxisAlignedBB(0.438, 0.312, 0, 0.562, 0.438, 0.062);
    private static final AxisAlignedBB CUBE8 = new AxisAlignedBB(0.25, 0.875, 0.25, 0.75, 1, 0.562);
    private static final AxisAlignedBB CUBE9 = new AxisAlignedBB(0.312, 0.75, 0.125, 0.688, 0.812, 0.938);
    private static final AxisAlignedBB CUBE10 = new AxisAlignedBB(0.312, 0.812, 0.25, 0.688, 0.938, 0.938);
    private static final AxisAlignedBB CUBE11 = new AxisAlignedBB(0.25, 0.125, 0.062, 0.312, 0.188, 0.125);
    private static final AxisAlignedBB CUBE12 = new AxisAlignedBB(0.688, 0.125, 0.062, 0.75, 0.188, 0.125);

    private static final AxisAlignedBB BOUNDING_BOX = new AxisAlignedBB(0.25, 0, 0, 0.75, 1, 1);

    public Type8719() {
        super("product_type_8719", "uri.type8719.name", BOUNDING_BOX, CUBE0, CUBE1, CUBE2, CUBE3, CUBE4, CUBE5, CUBE6, CUBE7, CUBE8, CUBE9, CUBE10, CUBE11, CUBE12);
        setProperties("mass", "ca. 1200g",
                "sealing", "FKM, EPDM, FFKM",
                "protection", "IP65",
                "operatingvoltage", "24V DC");
    }
}

package de.buerkert.flowcontrol.block;

import de.buerkert.flowcontrol.Rotations;
import net.minecraft.block.Block;
import net.minecraft.block.BlockHorizontal;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyDirection;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.Mirror;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FacingBlock extends Block {

    public static final PropertyDirection FACING = BlockHorizontal.FACING;
    private static final AxisAlignedBB FULLBLOCK_BOX = new AxisAlignedBB(0, 0, 0, 1, 1, 1);
    private AxisAlignedBB boundingBox;
    private AxisAlignedBB boundingBoxDump;
    private List<AxisAlignedBB> collisionBoxes;
    private List<AxisAlignedBB> collisionList = new ArrayList<>();
    private boolean isFullBlock;

    public FacingBlock(String name, Material material, CreativeTabs creativeTabs, float hardness, boolean isFull) {
        super(material);
        this.isFullBlock = isFull;
        if(isFull) {
            name = "block_" + name;
            boundingBoxDump = FULLBLOCK_BOX;
            collisionList.clear();
            collisionList.add(FULLBLOCK_BOX);
        }
        setRegistryName(name);
        setUnlocalizedName(name);
        setCreativeTab(creativeTabs);
        setHardness(hardness);
    }

    public FacingBlock(String name, Material material, CreativeTabs creativeTabs, float hardness, boolean isFull, String harvestTool, int harvestLevel) {
        this(name, material, creativeTabs, hardness, isFull);
        setHarvestLevel(harvestTool, harvestLevel);
    }

    public FacingBlock(String name, Material material, CreativeTabs creativeTabs, float hardness, AxisAlignedBB bounding_Box, AxisAlignedBB... collision_Boxes) {
        this(name, material, creativeTabs, hardness, false);
        boundingBoxDump = bounding_Box;
        collisionList.addAll(Arrays.asList(collision_Boxes));
    }

    public FacingBlock(String name, Material material, CreativeTabs creativeTabs, float hardness, String harvestTool, int harvestLevel, AxisAlignedBB bounding_Box, AxisAlignedBB... collision_Boxes) {
        this(name, material, creativeTabs, hardness, bounding_Box, collision_Boxes);
        setHarvestLevel(harvestTool, harvestLevel);
    }

    @Override
    public boolean isFullBlock(IBlockState state) {
        return this.isFullBlock;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return this.isFullBlock;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return this.isFullBlock;
    }

    @Override
    public BlockRenderLayer getBlockLayer() {
        return BlockRenderLayer.CUTOUT_MIPPED;
    }

    @Override
    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos) {
        return boundingBox;
    }

    @Override
    public void addCollisionBoxToList(IBlockState state, World worldIn, BlockPos pos, AxisAlignedBB entityBox, List<AxisAlignedBB> collidingBoxes, @Nullable Entity entity, boolean isActualstate) {
        setDefaultFacing(worldIn, pos, state);
        if(collisionBoxes.size() == 1) {
            addCollisionBoxToList(pos, entityBox, collidingBoxes, collisionBoxes.get(0));
            return;
        }
        entityBox = entityBox.offset(-pos.getX(), -pos.getY(), -pos.getZ());
        for(AxisAlignedBB box : collisionBoxes) {
            if(entityBox.intersects(box)) collidingBoxes.add(box.offset(pos));
        }
    }

    @Override
    public void onBlockAdded(World worldIn, BlockPos pos, IBlockState state) {
        setDefaultFacing(worldIn, pos, state);
    }

    @Override
    @Nullable
    public RayTraceResult collisionRayTrace(IBlockState state, World world, BlockPos pos, Vec3d start, Vec3d end) {
        setDefaultFacing(world, pos, state);
        double distanceSq;
        double distanceSqShortest = Double.POSITIVE_INFINITY;
        RayTraceResult resultClosest = null;
        RayTraceResult result;
        start = start.subtract(pos.getX(), pos.getY(), pos.getZ());
        end = end.subtract(pos.getX(), pos.getY(), pos.getZ());

        for(AxisAlignedBB box : collisionBoxes) {
            result = box.calculateIntercept(start, end);
            if(result == null) continue;

            distanceSq = result.hitVec.squareDistanceTo(start);
            if(distanceSq < distanceSqShortest) {
                distanceSqShortest = distanceSq;
                resultClosest = result;
            }
        }
        return resultClosest == null ? null : new RayTraceResult(RayTraceResult.Type.BLOCK, resultClosest.hitVec.addVector(pos.getX(), pos.getY(), pos.getZ()), resultClosest.sideHit, pos);
    }

    @Override
    public IBlockState getStateFromMeta(int meta) {
        EnumFacing enumFacing = EnumFacing.getFront(meta);
        if(enumFacing.getAxis() == EnumFacing.Axis.Y) enumFacing = EnumFacing.NORTH;
        return getDefaultState().withProperty(FACING, enumFacing);
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        return ((EnumFacing)state.getValue(FACING)).getIndex();
    }

    @Override
    public IBlockState withRotation(IBlockState state, Rotation rot) {
        return state.withProperty(FACING, rot.rotate((EnumFacing)state.getValue(FACING)));
    }

    @Override
    public IBlockState withMirror(IBlockState state, Mirror mirrorIn) {
        return state.withRotation(mirrorIn.toRotation((EnumFacing)state.getValue(FACING)));
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer(this, new IProperty[] {FACING});
    }

    @Override
    public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack) {
        worldIn.setBlockState(pos, state.withProperty(FACING, placer.getHorizontalFacing().getOpposite()), 2);
    }

    private void setDefaultFacing(World worldIn, BlockPos pos, IBlockState state) {
        IBlockState blockStateNorth = worldIn.getBlockState(pos.north());
        IBlockState blockStateSouth = worldIn.getBlockState(pos.south());
        IBlockState blockStateWest = worldIn.getBlockState(pos.west());
        IBlockState blockStateEast = worldIn.getBlockState(pos.east());
        EnumFacing enumFacing = (EnumFacing)state.getValue(FACING);

        if(enumFacing == EnumFacing.NORTH && blockStateNorth.isFullBlock() && !blockStateSouth.isFullBlock()) enumFacing = EnumFacing.SOUTH;
        else if(enumFacing == EnumFacing.SOUTH && blockStateSouth.isFullBlock() && !blockStateNorth.isFullBlock()) enumFacing = EnumFacing.NORTH;
        else if(enumFacing == EnumFacing.WEST && blockStateWest.isFullBlock() && !blockStateEast.isFullBlock()) enumFacing = EnumFacing.EAST;
        else if(enumFacing == EnumFacing.EAST && blockStateEast.isFullBlock() && !blockStateWest.isFullBlock()) enumFacing = EnumFacing.WEST;

        collisionBoxes = Rotations.rotateAxisAlignedBB(enumFacing, collisionList);
        boundingBox = Rotations.rotateAxisAlignedBB(enumFacing, boundingBoxDump);

        worldIn.setBlockState(pos, state.withProperty(FACING, enumFacing), 2);

        /*
         * the last argument to setBlockState is a bit flag that tells Minecraft
         * whether or not to update the client side / notify neighbouring blocks / other things
         * and you almost always want to set to either 2 (notify client) or 3 (notify client AND neighbors, IIRC)
         */
    }
}

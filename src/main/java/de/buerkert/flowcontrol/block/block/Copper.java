package de.buerkert.flowcontrol.block.block;

import de.buerkert.flowcontrol.Main;
import de.buerkert.flowcontrol.block.FacingBlock;
import net.minecraft.block.material.Material;

public class Copper extends FacingBlock {
    public Copper() {
        super("copper", Material.ROCK, Main.BUERKERT_TAB, 3.0F, true, "pickaxe", 2);
    }
}

package de.buerkert.flowcontrol.block.block;

import de.buerkert.flowcontrol.Main;
import de.buerkert.flowcontrol.block.FacingBlock;
import net.minecraft.block.material.Material;

public class CopperOre extends FacingBlock {
    public CopperOre() {
        super("copper_ore", Material.ROCK, Main.BUERKERT_TAB, 3.0F, true, "pickaxe", 2);

    }
}

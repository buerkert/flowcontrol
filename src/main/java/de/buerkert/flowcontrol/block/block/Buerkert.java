package de.buerkert.flowcontrol.block.block;

import de.buerkert.flowcontrol.Main;
import de.buerkert.flowcontrol.block.FacingBlock;
import net.minecraft.block.material.Material;

public class Buerkert extends FacingBlock {
    public Buerkert() {
        super("buerkert", Material.ROCK, Main.BUERKERT_TAB,5.0F, true);
    }
}

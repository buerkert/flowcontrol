package de.buerkert.flowcontrol.block;

import de.buerkert.flowcontrol.Main;
import de.buerkert.flowcontrol.flowsystem.IFlowBlockState;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;

import javax.annotation.Nullable;

public class FlowBlock extends Block {

    public FlowBlock(Material material) {
        super(material);
        setCreativeTab(Main.FLOW_SYSTEM_TAB);
    }

    public boolean canProvideWater(IBlockState blockState) {
        return false;
    }

    public boolean canConnectToTube(IFlowBlockState state, IBlockAccess world, BlockPos pos, @Nullable EnumFacing side) {
        return state.canProvideWater() && side != null;
    }
}

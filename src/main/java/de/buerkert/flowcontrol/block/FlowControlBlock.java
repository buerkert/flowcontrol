package de.buerkert.flowcontrol.block;

import de.buerkert.flowcontrol.block.block.Buerkert;
import de.buerkert.flowcontrol.block.block.Copper;
import de.buerkert.flowcontrol.block.block.CopperOre;
import de.buerkert.flowcontrol.block.flowsystem.Outlet;
import de.buerkert.flowcontrol.block.flowsystem.Pump;
import de.buerkert.flowcontrol.block.flowsystem.Repeater;
import de.buerkert.flowcontrol.block.flowsystem.Tube;
import de.buerkert.flowcontrol.block.product.Type0256;
import de.buerkert.flowcontrol.block.product.Type8719;
import de.buerkert.flowcontrol.block.product.Type8742;
import net.minecraft.block.Block;

import java.util.HashMap;
import java.util.Map;

public enum FlowControlBlock {
    BUERKERT("buerkert", new Buerkert()),
    COPPER("copper", new Copper()),
    COPPER_ORE("copper_ore", new CopperOre()),
    PUMP("flow_pump", new Pump()),
    TUBE("flow_tube", new Tube()),
    REPEATER("flow_repeater", new Repeater()),
    OUTLET("flow_outlet", new Outlet()),
    TYPE0256("product_type_0256", new Type0256()),
    TYPE8719("product_type_8719", new Type8719()),
    TYPE8742("product_type_8742", new Type8742())
    ;

    private static final Map<String, FlowControlBlock> BLOCK_MAP = new HashMap<>();
    private String name;
    private Block block;

    static {
        for(FlowControlBlock block : values()) {
            BLOCK_MAP.put(block.name, block);
        }
    }

    FlowControlBlock(String name, Block block) {
        this.name = name;
        this.block = block;
    }

    @Override
    public String toString() {
        return this.getName();
    }

    private String getName() {
        return this.name;
    }

    public Block getBlock() {
        return block;
    }

    public void setBlock(Block block) {
        this.block = block;
    }

    public static FlowControlBlock fromName(String name) {
        for(Map.Entry<String, FlowControlBlock> entry : BLOCK_MAP.entrySet()) {
            if (!entry.getKey().equalsIgnoreCase(name)) {
                continue;
            }
            return entry.getValue();
        }
        return null;
    }
}
